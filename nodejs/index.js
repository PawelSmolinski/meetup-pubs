'use strict';

var Aws = require('aws-sdk');
var Uuid = require('uuid');

var db = new Aws.DynamoDB({
    region: "eu-west-1"
});

var dbPubsTable = 'meetup-pubs';
var dbBeersTable = 'meetup-beers';

function mapPubItem(pubItem)
{
    return {
        id: pubItem.id.S,
        name: pubItem.name.S,
        city: pubItem.city.S
    };
}

function mapBeerItem(beerItem)
{
    return {
        id: beerItem.id.S,
        pub_id: beerItem.pub_id.S,
        name: beerItem.name.S,
        style: beerItem.style.S,
        price: beerItem.price.N
    };
}

exports.listPubsHandler = function(event, context, callback)
{
    db.scan({TableName: dbPubsTable}, function(error, data)
    {
        if(error){
            callback(error);
        } else {
            callback(null, data.Items.map(mapPubItem));
        }
    });
};

exports.getPubHandler = function(event, context, callback)
{
    console.log('### EVENT: ' + JSON.stringify(event));
    console.log('### CONTEXT: ' + JSON.stringify(context));

    if(typeof event.params.pub_id != 'string'){
        context.fail('Please provide pub ID');
    }

    var dbParams = {
        KeyConditionExpression: "#id = :pub_id",
        ExpressionAttributeNames: {
            "#id": "id"
        },
        ExpressionAttributeValues: {
            ":pub_id": {S: event.params.pub_id}
        },
        TableName: dbPubsTable
    };

    db.query(dbParams, function(error, data)
    {
        if(error){
            callback(error);
        } else {
            if(data.Count == 0){
                context.fail('Requested pub has not been found');
            } else {
                callback(null, mapPubItem(data.Items[0]));
            }
        }
    });

};

exports.createPubHandler = function(event, context, callback)
{
    console.log('### EVENT: ' + JSON.stringify(event));
    console.log('### CONTEXT: ' + JSON.stringify(context));

    if(typeof event.body.name != 'string'){
        context.fail('Pub name must be a string');
        return;
    }

    if(typeof event.body.city != 'string'){
        context.fail('City name must be a string');
        return;
    }

    var dbParams = {
        Item: {
            id: {S: Uuid.v4()},
            name: {S: event.body.name},
            city: {S: event.body.city}
        },
        TableName: dbPubsTable,
        ConditionExpression: "attribute_not_exists(id)"
    };

    db.putItem(dbParams, function(error)
    {
        if(error) {
            callback(error);
        } else {
            callback(null, mapPubItem(dbParams.Item));
        }
    });

};

exports.listBeersHandler = function(event, context, callback)
{
    if(typeof event.params.pub_id != 'string'){
        context.fail('Please provide valid pub ID');
        return;
    }

    var dbParams = {
        KeyConditionExpression: "#pub_id = :pub_id",
        ExpressionAttributeNames: {
            "#pub_id": "pub_id"
        },
        ExpressionAttributeValues: {
            ":pub_id": {S: event.params.pub_id}
        },
        TableName: dbBeersTable
    };

    db.query(dbParams, function(error, data)
    {
        if(error){
            callback(error);
        } else {
            callback(null, data.Items.map(mapBeerItem));
        }
    });
};

exports.getSingleBeerHandler = function(event, context, callback)
{
    if(typeof event.params.pub_id != 'string'){
        context.fail('Please provide valid pub ID');
        return;
    }

    if(typeof event.params.beer_id != 'string'){
        context.fail('Please provide valid beer ID');
        return;
    }

    var dbParams = {
        KeyConditionExpression: "#pub_id = :pub_id AND #id = :beer_id",
        ExpressionAttributeNames: {
            "#pub_id": "pub_id",
            "#id": "id"
        },
        ExpressionAttributeValues: {
            ":pub_id": {S: event.params.pub_id},
            ":beer_id": {S: event.params.beer_id}
        },
        TableName: dbBeersTable
    };

    db.query(dbParams, function(error, data)
    {
        if(error){
            callback(error);
        } else {
            if(data.Count == 0) {
                context.fail('Requested beer has not been found');
            } else {
                callback(null, mapBeerItem((data.Items[0])));
            }
        }
    });
};

exports.createBeerHandler = function(event, context, callback)
{
    if(typeof event.params.pub_id != 'string'){
        context.fail('Please provide valid pub ID');
        return;
    }

    if(typeof event.body.name != 'string'){
        context.fail('Please provide beer name');
        return;
    }

    if(typeof event.body.style != 'string'){
        context.fail('Please provide beer style');
    }

    if(typeof event.body.price == 'undefined'){
        context.fail('Please provide beer price');
    }

    var dbParams = {
        Item: {
            id: {S: Uuid.v4()},
            pub_id: {S: event.params.pub_id},
            name: {S: event.body.name},
            style: {S: event.body.style},
            price: {N: event.body.price.toString()}
        },
        TableName: dbBeersTable,
        ConditionExpression: "attribute_not_exists(id)"
    };

    db.putItem(dbParams, function(error)
    {
        if(error) {
            callback(error);
        } else {
            callback(null, mapBeerItem(dbParams.Item));
        }
    });
};

exports.updateBeerPriceHandler = function(event, context, callback)
{
    if(typeof event.params.pub_id != 'string'){
        context.fail('Please provide valid pub ID');
        return;
    }

    if(typeof event.params.beer_id != 'string'){
        context.fail('Please provide valid beer ID');
        return;
    }

    if(typeof event.body.price == 'undefined'){
        context.fail('Please provide beer price');
    }

    var dbParams = {
        Key: {
            pub_id: {S: event.params.pub_id},
            id: {S: event.params.beer_id}
        },
        UpdateExpression: "SET #price = :price",
        ExpressionAttributeNames: {
            "#price": "price"
        },
        ExpressionAttributeValues: {
            ":price": {N: event.body.price.toString()}
        },
        TableName: dbBeersTable
    };

    db.updateItem(dbParams, function(error)
    {
        if(error){
            callback(error);
        } else {
            exports.getSingleBeerHandler(event, context, callback);
        }
    });
};

exports.deleteBeerHandler = function(event, context, callback)
{
    if(typeof event.params.pub_id != 'string'){
        context.fail('Please provide valid pub ID');
        return;
    }

    if(typeof event.params.beer_id != 'string'){
        context.fail('Please provide valid beer ID');
        return;
    }

    var dbParams = {
        Key: {
            pub_id: {S: event.params.pub_id},
            id: {S: event.params.beer_id}
        },
        TableName: dbBeersTable
    };

    db.deleteItem(dbParams, function(error)
    {
        if(error) {
            callback(error);
        } else {
            callback(null, {});
        }
    });
};
