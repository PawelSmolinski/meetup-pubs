from troposphere import Ref, Template, Output
from troposphere.dynamodb import Table, ProvisionedThroughput
from troposphere.dynamodb import AttributeDefinition, KeySchema
from troposphere.apigateway import RestApi, Method
from troposphere.apigateway import Resource, MethodResponse
from troposphere.apigateway import Integration, IntegrationResponse
from troposphere.apigateway import Deployment, Stage
from troposphere.apigateway import ApiKey, StageKey
from troposphere.iam import Role, Policy
from troposphere.awslambda import Function, Code
from troposphere import GetAtt, Join


# How HTTP request (body, query string params, path params) should be passed to lambda function
# {
#   body: <raw POST/PUT body>
#   method: <HTTP method>
#   headers: <HTTP headers>
#   params: <Path parameters>
#   query: <Query string parameters>
# }
api_gateway_request_template = """{
  "body" : $input.json('$'),
  "headers": {
    #foreach($header in $input.params().header.keySet())
    "$header": "$util.escapeJavaScript($input.params().header.get($header))" #if($foreach.hasNext),#end

    #end
  },
  "method": "$context.httpMethod",
  "params": {
    #foreach($param in $input.params().path.keySet())
    "$param": "$util.escapeJavaScript($input.params().path.get($param))" #if($foreach.hasNext),#end

    #end
  },
  "query": {
    #foreach($queryParam in $input.params().querystring.keySet())
    "$queryParam": "$util.escapeJavaScript($input.params().querystring.get($queryParam))" #if($foreach.hasNext),#end

    #end
  }
}"""

# Template object which will contain whole configuration
t = Template()

# Two DynamoDB tables
# meetup-pubs will store info about pubs
# Note: May omit table name - will be auto-generated with unique name (launch multiple copies of the
# same configuration will not raise DB table unique name error)
# Note2: DynamoDB is schema-less, we have to define only those attributes which will work as indexes
t.add_resource(Table("PubsTable",
                     TableName="meetup-pubs",
                     AttributeDefinitions=[
                         AttributeDefinition(
                             AttributeName="id",
                             AttributeType="S"
                         )
                     ],
                     KeySchema=[KeySchema(
                         AttributeName="id",
                         KeyType="HASH"
                     )],
                     ProvisionedThroughput=ProvisionedThroughput(
                         ReadCapacityUnits=5,
                         WriteCapacityUnits=5
                     )))

# ...and second table for beers
# Note: pub_id as partition key (internal storage location within DynamoDB)
t.add_resource(Table("BeersTable",
                     TableName="meetup-beers",
                     AttributeDefinitions=[
                         AttributeDefinition(
                             AttributeName="id",
                             AttributeType="S"
                         ),
                         AttributeDefinition(
                             AttributeName="pub_id",
                             AttributeType="S"
                         )
                     ],
                     KeySchema=[
                         KeySchema(
                             AttributeName="pub_id",
                             KeyType="HASH"
                         ),
                         KeySchema(
                             AttributeName="id",
                             KeyType="RANGE"
                         )
                     ],
                     ProvisionedThroughput=ProvisionedThroughput(
                         ReadCapacityUnits=5,
                         WriteCapacityUnits=5
                     )))

# API gateway
rest_api = t.add_resource(RestApi("MeetupApi",
                                  Name="MeetupApi",
                                  DependsOn=["PubsTable", "BeersTable"]
                                  ))

# Create a role for the lambda function. Will create IAM user with access rights requested.
# We need:
# * be able to execute Lambda functions
# * operate on DynamoDB (in this sample every table within account)
# * have possibility to access logging system
t.add_resource(Role(
    "LambdaExecutionRole",
    Path="/",
    Policies=[Policy(PolicyName="root",
                     PolicyDocument={
                         "Version": "2012-10-17",
                         "Statement": [{
                             "Action": ["logs:*"],
                             "Resource": "arn:aws:logs:*:*:*",
                             "Effect": "Allow"
                         }, {
                             "Action": ["lambda:*"],
                             "Resource": "*",
                             "Effect": "Allow"
                         }, {
                             "Action": ["dynamodb:*"],
                             "Effect": "Allow",
                             "Resource": [
                                 {"Fn::Join": ["", ["arn:aws:dynamodb:*:", {"Ref": "AWS::AccountId"}, ":table/*"]]}
                             ]
                         }]
                     })],
    AssumeRolePolicyDocument={"Version": "2012-10-17", "Statement": [
        {
            "Action": ["sts:AssumeRole"],
            "Effect": "Allow",
            "Principal": {
                "Service": [
                    "lambda.amazonaws.com",
                    "apigateway.amazonaws.com",
                    "dynamodb.amazonaws.com"
                ]
            }
        }
    ]},
))

# Resource for pubs collection, accessible via API_ROOT_URL/pubs
pubs_resource = t.add_resource(Resource("PubsResource",
                               RestApiId=Ref(rest_api),
                               PathPart="pubs",
                               ParentId=GetAtt("MeetupApi", "RootResourceId")
                               ))

# Resource for single pub data, accessible via API_ROOT_URL/pubs/{pub_id}
# (curly braces are used to define path parameter)
single_pub_resource = t.add_resource(Resource("SinglePubResource",
                                              RestApiId=Ref(rest_api),
                                              ParentId=Ref("PubsResource"),
                                              PathPart="{pub_id}"
                                              ))

# Resource for beers collection, accessible via API_ROOT_URL/pubs/{pub_id}/beers
# Note: may be accessible via API_ROOT_URL/beers when we will change ParentId to root API resource
beers_resource = t.add_resource(Resource("BeersResource",
                                         RestApiId=Ref(rest_api),
                                         PathPart="beers",
                                         ParentId=Ref("SinglePubResource")
                                         ))

# Resource for single beer, accessible via API_ROOT_URL/pubs/{pub_id}/beers/{beer_id}
single_beer_resource = t.add_resource(Resource("SingleBeerResource",
                                               RestApiId=Ref(rest_api),
                                               PathPart="{beer_id}",
                                               ParentId=Ref("BeersResource")
                                               ))

###
### Here come definitions for lambda functions and API gateway methods associated with them
### S3 bucket meetup-api contains meetup-api.zip file which is is roughly speaking
### our Node.JS app main directory packed into single ZIP file, i.e. its contents is:
### ./index.js
### ./package.json
### ./node_modules/*
###


# Lambda function for listing all pubs available
# Note: Handler value is in the form: moduleName.exportedFunctionName
t.add_resource(Function("ListPubsFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.listPubsHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

# API Gateway method - GET call on pubs resource will redirect request to
# Lambda function defined above.
# Note: we can modify both request params/body as well as response received from Lambda
# (in here we are using request template described above and returning data from Lambda
# w/o modification, but always with HTTP status 200, even when error will occur ;))
t.add_resource(Method(
    "ListPubsMethod",
    DependsOn='ListPubsFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(pubs_resource),
    HttpMethod="GET",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("ListPubsFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))

# Rest of definitions... look the same as the sample above. Should be some method defined... laziness over DRY :)
t.add_resource(Function("GetPubFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.getPubHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "GetPubMethod",
    DependsOn='GetPubFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(single_pub_resource),
    HttpMethod="GET",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        RequestTemplates={
          "application/json": api_gateway_request_template
        },
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("GetPubFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))


t.add_resource(Function("CreatePubFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.createPubHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "CreatePubMethod",
    DependsOn='CreatePubFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(pubs_resource),
    HttpMethod="POST",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        RequestTemplates={
            "application/json": api_gateway_request_template
        },
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("CreatePubFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))

t.add_resource(Function("ListBeersFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.listBeersHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "ListBeersMethod",
    DependsOn='ListBeersFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(beers_resource),
    HttpMethod="GET",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        RequestTemplates={
            "application/json": api_gateway_request_template
        },
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("ListBeersFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))

t.add_resource(Function("GetBeerFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.getSingleBeerHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "GetBeerMethod",
    DependsOn='GetBeerFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(single_beer_resource),
    HttpMethod="GET",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        RequestTemplates={
            "application/json": api_gateway_request_template
        },
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("GetBeerFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))


t.add_resource(Function("CreateBeerFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.createBeerHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "CreateBeerMethod",
    DependsOn='CreateBeerFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(beers_resource),
    HttpMethod="POST",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        RequestTemplates={
            "application/json": api_gateway_request_template
        },
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("CreateBeerFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))


t.add_resource(Function("UpdateBeerPriceFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.updateBeerPriceHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "UpdateBeerPriceMethod",
    DependsOn='UpdateBeerPriceFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(single_beer_resource),
    HttpMethod="PUT",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        RequestTemplates={
            "application/json": api_gateway_request_template
        },
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("UpdateBeerPriceFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))

t.add_resource(Function("DeleteBeerFunction",
                        Code=Code(S3Bucket="meetup-api", S3Key="meetup-api.zip"),
                        Handler="index.deleteBeerHandler",
                        Role=GetAtt("LambdaExecutionRole", "Arn"),
                        Runtime="nodejs4.3"
                        ))

t.add_resource(Method(
    "DeleteBeerMethod",
    DependsOn='DeleteBeerFunction',
    RestApiId=Ref(rest_api),
    AuthorizationType="NONE",
    ResourceId=Ref(single_beer_resource),
    HttpMethod="DELETE",
    Integration=Integration(
        Credentials=GetAtt("LambdaExecutionRole", "Arn"),
        Type="AWS",
        IntegrationHttpMethod='POST',
        RequestTemplates={
            "application/json": api_gateway_request_template
        },
        IntegrationResponses=[
            IntegrationResponse(StatusCode='200')
        ],
        Uri=Join("", [
            "arn:aws:apigateway:eu-west-1:lambda:path/2015-03-31/functions/",
            GetAtt("DeleteBeerFunction", "Arn"),
            "/invocations"
        ])
    ),
    MethodResponses=[
        MethodResponse("CatResponse", StatusCode='200')
    ]
))

# API Gateway stages making development easier (you may have e.g. dev, staging & production stages)
# You may pass different configuration for every stage via so called stage variables
stage_name = 'v1'

# Deploy our API.
# Note: all API methods must be included in DependsOn
deployment = t.add_resource(Deployment("%sDeployment" % stage_name,
    DependsOn=["ListPubsMethod", "GetPubMethod", "CreatePubMethod", "ListBeersMethod", "GetBeerMethod",
               "CreateBeerMethod", "UpdateBeerPriceMethod", "DeleteBeerMethod"],
    RestApiId=Ref(rest_api),
    ))

stage = t.add_resource(Stage('%sStage' % stage_name,
    StageName=stage_name,
    RestApiId=Ref(rest_api),
    DeploymentId=Ref(deployment)
))

# Output will give us link to our API endpoint
t.add_output([
    Output("ApiEndpoint",
        Value=Join("", [
            "https://",
            Ref(rest_api),
            ".execute-api.eu-west-1.amazonaws.com/",
            stage_name
        ]),
        Description="Endpoint for this stage of the api"
    ),
])

# As configuration is completed, we just need to call to_json on template object
# and use result in AWS
print(t.to_json())
